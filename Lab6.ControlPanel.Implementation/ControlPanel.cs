﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
    public class ControlPanel : IControlPanel
    {
        public IMycie mycie;
        private Window window;
        public ControlPanel(IMycie mycie)
        {
            this.mycie = mycie;
            window = new GUI(mycie);
        }

        public System.Windows.Window Window
        {
            get { return window; }

        }



    }
}
