﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab6.MainComponent.Contract;

namespace Lab6.ControlPanel.Implementation
{
    /// <summary>
    /// Interaction logic for GUI.xaml
    /// </summary>
    partial class GUI : Window
    {
        IMycie myjnia;
        public GUI(IMycie myjnia)
        {
            InitializeComponent();
            this.myjnia = myjnia;
        }
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            myjnia.Myj();


        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            myjnia.Susz();
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            myjnia.Woskuj();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.InvokeShutdown();
        }
    }
}
