﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.MainComponent.Contract;
using Lab6.Display.Contract;

namespace Lab6.MainComponent.Implementation
{
    public class Myjnia : IMycie
    {
        IDisplay wyswietlacz;
        public Myjnia(IDisplay wyswietlacz)
        {
            this.wyswietlacz = wyswietlacz;
        }
       
          

        string IMycie.Myj()
        {
            string t = "Myje Fure";
            wyswietlacz.Text = t;
            return t;
        }

        string IMycie.Woskuj()
        {
            string t = "Woskuje Fure";
            wyswietlacz.Text = t;
            return t;
        }

        string IMycie.Susz()
        {
            string t = "Susze Fure";
            wyswietlacz.Text = t;
            return t;
        }
   
    }
}